import random as r


class GAME:
    def __init__(self, name, digit):
        self.name = name
        self.digit = digit

    def generate_no(self):
        self.no = r.randrange(1, pow(10, self. digit))

    def high_or_low(self, n):
        self.msg = ""
        if n < self.no - pow(10, self.digit-1):
            self.msg = "Too Low"
            return 0
        elif n >= self.no - pow(10, self.digit-1) and n < self.no:
            self.msg = "Low"
            return 0
        elif n > self.no + pow(10, self.digit-1):
            self.msg = "Too High"
            return 0
        elif n > self.no and n <= self.no + pow(10, self.digit-1):
            self.msg = "High"
            return 0
        elif n == self.no:
            return 1

    def correct_nos(self, n):
        s = str(self.no)
        s1 = str(n)
        count = 0
        for i in range(len(s1)):
            if s1[i] in s:
                count += 1
                a = s.index(s1[i])
                s = s[:a]+s[a+1:]
        self.msg = self.msg+"	,	"+str(count)+" digits are correct"

    def correct_pos(self, n):
        s = str(n)
        s1 = str(self.no)
        count = 0
        x = len(s)
        if len(s1) < len(s):
            x = len(s1)
        i = 1
        while x != 0:
            if s[-i] == s1[-i]:
                count += 1
            i += 1
            x -= 1

        self.msg = self.msg+"	,	"+str(count)+" digits are in correct position"
        return self.msg, self.no
